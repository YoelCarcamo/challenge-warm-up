import React, { useContext, useEffect } from 'react';
import { useFormik } from 'formik';
import { Link, useHistory } from 'react-router-dom';


import { validate } from '../validaciones/validarLogin';
import authContext from '../context/autenticacion/authContext';
import AlertaContext from '../context/alerta/alertaContext';


const Login = () => {

    const { mensaje, autenticado, mensajeNro, iniciarSesion, comprobarAutenticacion } = useContext(authContext);

    const { alerta, mostrarAlerta } = useContext(AlertaContext);

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validate,
        onSubmit: values => {
            iniciarSesion( values )
        }
    })

    const history = useHistory();

    useEffect(() => {
        //si esta autenticado, redigir al home
        if(autenticado){
            history.push('/')
        }

        //si no esta autenticado pero tiene el token en el localStorage
        const token = localStorage.getItem('token');

        comprobarAutenticacion(token)
        // eslint-disable-next-line
    }, [autenticado])

    useEffect(() => {
        if(mensaje){
            mostrarAlerta(mensaje)
        }
        // eslint-disable-next-line
    }, [mensajeNro])

    return ( 
        <div className='pantalla-full bg-azul d-flex justify-content-center align-items-center'>
            <form 
                className='bg-light px-4 py-4 form rounded'
                onSubmit={formik.handleSubmit}
            >
                <img src='/usuario.png' className='mx-auto d-block mb-3 img-usuario' alt='usuario'/>
                <div className='input-group'>
                    <input 
                        type='email'
                        name='email'
                        placeholder='Email'
                        className='form-control'
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.email}
                    ></input>
                </div>
                {formik.errors.email 
                ? 
                    <p className='text-danger'>{formik.errors.email}</p> 
                : 
                    null
                }

                <div className='input-group mt-3'>
                    <input 
                        type='password'
                        name='password'
                        placeholder='Password'
                        className='form-control'
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.password}
                    ></input>
                </div>
                {formik.errors.password 
                ? 
                    <p className='text-danger'>{formik.errors.password}</p> 
                : 
                    null
                }

                {!alerta ?  null : <div className='alert alert-danger my-2'>{mensaje}</div>}

                <button
                    type='submit'
                    className='btn btn-primary w-100 mt-3'
                >Iniciar Sesión</button>
                
                <div className='my-2 justify-content-end d-flex'>
                    <Link to='/'>Registrarse</Link>
                </div>
                
            </form>
        </div>
    );
}
 
export default Login;