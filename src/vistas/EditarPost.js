import React, { useContext, useEffect, useState } from 'react';
import { useFormik } from 'formik';

import Footer from '../componentes/layout/Footer';
import Header from '../componentes/layout/Header';
import PostContext from '../context/post/postContext';
import Spinner from '../componentes/layout/Spinner';
import {validate} from '../validaciones/validarPosteo';


const EditarPost = props => {
    const { posteoTraido, obtenerPosteoPorId, editarPosteo } = useContext(PostContext);
    const [ cargando, setCargando ] = useState(true)

    //obtener el id de la URL
    const idUrl = props.location.pathname.split('/')[3]

    useEffect( () => {
        const consultarApi = async () => {
            //obtenemos el posteo que queremos editar por el id
            await obtenerPosteoPorId(idUrl);
            setCargando(false);
        }

        consultarApi()
        // eslint-disable-next-line
    }, [])


    useEffect( () => {
        //establecemos los textos del posteo a editar en el formulario, cuando se obtiene el posteo de la api
        formik.setFieldValue("title", posteoTraido.title, false)
        formik.setFieldValue("body", posteoTraido.body, false)
        // eslint-disable-next-line
    }, [posteoTraido])

    const formik = useFormik({
        //iniciamos el formulario, ya con los textos del posteo a editar
        initialValues: {
            title: '',
            body: ''
        },
        validate,
        onSubmit: async values => {
            //Le pasamos el posteo editado mas el id
            await editarPosteo({...values, id: idUrl});

            //una vez creado, redirigimos al home
            props.history.push('/');
        }
    })


    return ( 
        <>
        <div class='alto-menos-footer'>
            <Header/>
            <main className='container justify-content-center d-flex'>
                {/* Mostramos el spinner mientras hace la peticion para buscar el posteo*/}
                { cargando ?
                <Spinner/>
                :
                (
                    // Una vez obtenido la respuesta de la peticion,
                    // mostramos el formulario con el posteo en caso de que exista
                    Object.keys(posteoTraido).length === 0 ?
                        <h5 className='text-center'>El post con el id {idUrl} no existe</h5>
                    :
                        <form
                            className='my-4 form-post'
                            onSubmit={formik.handleSubmit}
                        >
                            <h2 className='text-center text-primary'>Editar Post</h2>

                            <div className='input-group'>
                                <input 
                                    type='text'
                                    name='title'
                                    placeholder='Titulo'
                                    className='form-control'
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.title}
                                ></input>
                            </div>
                            {formik.errors.title 
                            ? 
                                <p className='text-danger'>{formik.errors.title}</p> 
                            : 
                                null
                            }

                            <div className='input-group mt-3'>
                                <textarea
                                    name='body'
                                    placeholder='Descripción'
                                    className='form-control'
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.body}
                                ></textarea>
                            </div>
                            {formik.errors.body 
                            ? 
                                <p className='text-danger'>{formik.errors.body}</p> 
                            : 
                                null
                            }

                            {/* {error ? <div className='alert alert-danger'></div> : null} */}
                            <button
                                type='submit'
                                className='btn btn-primary w-100 mt-3'
                            >Editar Post</button>
                        </form>
                )
                }

            </main>
        </div>
        <Footer/>
        </>
     );
}
 
export default EditarPost;