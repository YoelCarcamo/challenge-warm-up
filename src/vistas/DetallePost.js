import React, { useContext, useEffect, useState } from 'react';


import Footer from '../componentes/layout/Footer';
import Header from '../componentes/layout/Header';
import Spinner from '../componentes/layout/Spinner';
import PostContext from '../context/post/postContext';


const DetallePost = props => {

    const { posteoTraido, obtenerPosteoPorId } = useContext(PostContext);
    const [ cargando, setCargando ] = useState(true)

    //obtener el id de la URL
    const idUrl = props.location.pathname.split('/')[3]

    useEffect( () => {
        const consultarApi = async () => {
            await obtenerPosteoPorId(idUrl);
            setCargando(false);
        }

        consultarApi()
        // eslint-disable-next-line
    }, [])

    const { userId, id, title, body } = posteoTraido;

    return ( 
        <>
        <div className='alto-menos-footer'>
            <Header/>
            <main className='container py-3'>
                {/* Mostramos el spinner mientras hace la peticion para buscar el posteo*/}
                { cargando ?
                <Spinner/>
                :
                // Una vez obtenido la respuesta de la peticion,
                // mostramos el posteo en caso de que exista
                (
                    Object.keys(posteoTraido).length === 0 ?
                        <h5 className='text-center'>El post con el id {idUrl} no existe</h5>
                    :
                    <section className='border p-3'>
                        <h2>#{id}</h2>
                        <h2 className='text-primary'>{title}</h2>
                        <p className="lead">{body}</p>
                        <p className='text-right'><cite>Id del creador del post: {userId}</cite></p>
                    </section>
                )   
                }
            </main>
        </div>
        <Footer/>
        </>
    );
}
 
export default DetallePost;