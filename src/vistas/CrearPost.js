import React, { useContext } from 'react';
import { useFormik } from 'formik';
import { useHistory } from 'react-router';

import Footer from '../componentes/layout/Footer';
import Header from '../componentes/layout/Header';
import PostContext from '../context/post/postContext';
import { validate } from '../validaciones/validarPosteo'


const CrearPost = () => {

    const { crearPosteo } = useContext(PostContext);

    const history = useHistory();

    const formik = useFormik({
        initialValues: {
            title: '',
            body: ''
        },
        validate,
        onSubmit: async values => {
            await crearPosteo(values)
            //una vez creado, redirigimos al home
            history.push('/');

        }
    })


    return ( 
        <>
        <div class='alto-menos-footer'>
            <Header/>
            <main className='container justify-content-center d-flex'>
                <form
                    className='my-4 form-post'
                    onSubmit={formik.handleSubmit}
                >
                    <h2 className='text-center text-primary'>Crear Post</h2>

                    <div className='input-group'>
                        <input 
                            type='text'
                            name='title'
                            placeholder='Titulo'
                            className='form-control'
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.title}
                        ></input>
                    </div>
                    {formik.errors.title 
                    ? 
                        <p className='text-danger'>{formik.errors.title}</p> 
                    : 
                        null
                    }

                    <div className='input-group mt-3'>
                        <textarea
                            name='body'
                            placeholder='Descripción'
                            className='form-control'
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.body}
                        ></textarea>
                    </div>
                    {formik.errors.body 
                    ? 
                        <p className='text-danger'>{formik.errors.body}</p> 
                    : 
                        null
                    }

                    {/* {error ? <div className='alert alert-danger'></div> : null} */}
                    <button
                        type='submit'
                        className='btn btn-primary w-100 mt-3'
                    >Crear Post</button>
                </form>
            </main>
        </div>
        <Footer/>
        </>
     );
}
 
export default CrearPost;