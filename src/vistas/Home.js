import React, { useEffect, useContext } from 'react';

import Footer from '../componentes/layout/Footer';
import Header from '../componentes/layout/Header';
import Post from '../componentes/post/Post';
import PostContext from '../context/post/postContext';

const Home = () => {

    const { posteos, obtenerTodosLosPosteos } = useContext(PostContext)

    useEffect(() => {
        obtenerTodosLosPosteos();
        // eslint-disable-next-line
    }, [])


    return ( 
        <>
            <div class='alto-menos-footer'>
                <Header/>
                <main className='container'>
                    <h2 className='text-primary mt-3'>Listado de Posteos</h2>
                    <section>
                        { posteos.length === 0 ?
                            <p className='text-center'>No se encontro posteos</p>
                        :
                            posteos.map( posteo => (
                                <Post posteo={posteo} key={posteo.id}/>
                            ))
                        } 

                    </section>
                </main>
            </div>
            <Footer/>
        </>
     );
}
 
export default Home;