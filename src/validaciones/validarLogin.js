exports.validate = valores => {
    const errors = {};

    if (!valores.email) {
        errors.email = 'Requerido';
      } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(valores.email)) {
        errors.email = 'Email invalido';
    }

    if(!valores.password) {
        errors.password = 'Requerido'
    }

    return errors;
}