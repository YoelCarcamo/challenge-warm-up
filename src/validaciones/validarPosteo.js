

exports.validate = valores => {
    const errors = {};

    if (!valores.title) {
        errors.title = 'Requerido';
    }

    if(!valores.body) {
        errors.body = 'Requerido'
    }

    return errors;
}