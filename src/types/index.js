export const OBTENER_TODOS_LOS_POSTEOS = 'OBTENER_TODOS_LOS_POSTEOS'
export const ELIMINAR_POSTEO = 'ELIMINAR_POSTEO';
export const CREAR_POSTEO = 'CREAR_POSTEO';
export const POSTEO_TRAIDO = 'POSTEO_TRAIDO';
export const POSTEO_NO_ENCONTRADO = 'POSTEO_NO_ENCONTRADO';
export const EDITAR_POSTEO = 'EDITAR_POSTEO';


export const CERRAR_SESION = 'CERRAR_SESION';
export const INICIO_SESION_EXITOSO = 'INICIO_SESION_EXITOSO';
export const INICIO_SESION_FALLIDO = 'INICIO_SESION_FALLIDO';
export const AUTENTICACION_VALIDA = 'AUTENTICACION_VALIDA';


export const MOSTRAR_ALERTA = 'MOSTRAR_ALERTA';
export const OCULTAR_ALERTA = 'OCULTAR_ALERTA';