import { 
  BrowserRouter as Router, 
  Route,
  Switch
} from 'react-router-dom';

import Login from "./vistas/Login";
import Home from './vistas/Home';
import PostState from './context/post/postState.js';
import DetallePost from './vistas/DetallePost';
import CrearPost from './vistas/CrearPost';
import EditarPost from './vistas/EditarPost';
import AuthState from './context/autenticacion/authState';
import RutaPrivada from './rutas/RutaPrivada';
import AlertaState from './context/alerta/alertaState';


function App() {

  return (
    <PostState>
      <AuthState>
        <AlertaState>
          <Router>
            <Switch>
              <RutaPrivada path='/' exact component={Home}/>
              <RutaPrivada path='/post/ver/:id' exact component={DetallePost}/>
              <RutaPrivada path='/post/editar/:id' exact component={EditarPost}/>
              <RutaPrivada path='/post/crear' exact component={CrearPost}/>
              <Route path='/login' exact component={Login}/>
            </Switch>
          </Router>
        </AlertaState>
      </AuthState>
    </PostState>
  );
}

export default App;
