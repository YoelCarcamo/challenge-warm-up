import { AUTENTICACION_VALIDA, CERRAR_SESION, INICIO_SESION_EXITOSO, INICIO_SESION_FALLIDO } from "../../types";

const authReducer = (state, action) => {
    switch(action.type){
        case INICIO_SESION_EXITOSO:
        case AUTENTICACION_VALIDA:
            return {
                ...state,
                autenticado: true,
                mensaje: null
            }
        case INICIO_SESION_FALLIDO:
            return {
                ...state,
                mensaje: action.payload,
                mensajeNro: state.mensajeNro + 1
            }
        case CERRAR_SESION: 
            return {
                ...state,
                autenticado: false,
                mensaje: null
            }
        default :
            return state;
    }
}

export default authReducer;