import axios from 'axios';
import { useReducer } from 'react'

import { AUTENTICACION_VALIDA, CERRAR_SESION, INICIO_SESION_EXITOSO, INICIO_SESION_FALLIDO } from "../../types";
import AuthContext from "./authContext";
import authReducer from "./authReducer";


const AuthState = props => {
    const stateInicial = {
        autenticado: false,
        mensaje: null,
        mensajeNro: 0
    }

    const [state, dispatch ] = useReducer(authReducer, stateInicial);

    const iniciarSesion = async credenciales => {
        try {
            const respuesta = await axios.post('http://challenge-react.alkemy.org/', credenciales);

            dispatch({
                type: INICIO_SESION_EXITOSO,
                payload: respuesta.data
            })

            //guardamos en el localStorage el token
            localStorage.setItem('token', respuesta.data.token)
        } catch (error) {
            console.log(error.response.data)

            dispatch({
                type: INICIO_SESION_FALLIDO,
                payload: error.response.data.error
            })

        }
    }


    const cerrarSesion = () => {
        //borramos el token del localStorage
        localStorage.removeItem('token');

        dispatch({
            type: CERRAR_SESION
        })
    }

    const comprobarAutenticacion = token => {
        if(!token){
            return;
        }
        
        try {
            //aca consultariamos en la api si el token es valido
            dispatch({
                type: AUTENTICACION_VALIDA
            })

        } catch (error) {
            console.log(error)
        }
    }

    return ( 
        <AuthContext.Provider
            value={{
                autenticado: state.autenticado,
                mensaje: state.mensaje,
                mensajeNro: state.mensajeNro,
                iniciarSesion,
                cerrarSesion,
                comprobarAutenticacion
            }}
        >
            {props.children}
        </AuthContext.Provider>
     );
}
 
export default AuthState;