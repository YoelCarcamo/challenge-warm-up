import { useReducer } from "react"
import { MOSTRAR_ALERTA, OCULTAR_ALERTA } from "../../types";
import AlertaContext from "./alertaContext";
import alertaReducer from "./alertaReducer"



const AlertaState = props => {

    const stateInicial = {
        alerta: null
    }

    const [state, dispatch] = useReducer(alertaReducer, stateInicial);

    const mostrarAlerta = (alerta) => {
        dispatch({
            type: MOSTRAR_ALERTA,
            payload: alerta
        })

        //ocultamos el alerta despues de 4 segundos
        setTimeout(()=>{
            ocultarAlerta()
        },4000);
    }

    const ocultarAlerta = () => {
        dispatch({
            type: OCULTAR_ALERTA
        })
    }

    return(

        <AlertaContext.Provider
            value={{
                alerta: state.alerta,
                mostrarAlerta
            }}
        >
            {props.children}
        </AlertaContext.Provider>
    )
}

export default AlertaState;