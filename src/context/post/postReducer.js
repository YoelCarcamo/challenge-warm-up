import { CREAR_POSTEO, EDITAR_POSTEO, ELIMINAR_POSTEO, OBTENER_TODOS_LOS_POSTEOS, POSTEO_NO_ENCONTRADO, POSTEO_TRAIDO } from "../../types";

const postReducer = (state, action) => {
    switch(action.type){
        case OBTENER_TODOS_LOS_POSTEOS:
            return {
                ...state,
                posteos: action.payload,
                mensaje: null
            }
        case ELIMINAR_POSTEO:
            return {
                ...state,
                //creamos un nuevo array con todos los posteos que sean diferente al id del post eliminado
                posteos: state.posteos.filter( posteo => posteo.id !== action.payload),
                mensaje: "Se ha eliminado correctamente el post",
                mensajeNro: state.mensajeNro + 1
            }
        case POSTEO_TRAIDO: 
            return {
                ...state,
                posteoTraido: action.payload,
                mensaje: null
            }
        case POSTEO_NO_ENCONTRADO:
            return {
                ...state,
                posteoTraido: {},
                mensaje: null
            }
        case CREAR_POSTEO:
            return {
                ...state,
                posteos: [...state.posteos, action.payload],
                mensaje: "Se ha creado correctamente el post",
                mensajeNro: state.mensajeNro + 1
            }
        case EDITAR_POSTEO:
            return {
                ...state,
                //actualizamos el posteo
                posteos: state.posteos.map( posteo => {
                    if( posteo === Number(action.payload.id)){
                        posteo.title = action.payload.title;
                        posteo.body = action.payload.body;
                    }

                    return posteo;
                }),
                mensaje: 'Se ha editado correctamente el post',
                mensajeNro: state.mensajeNro + 1
            }
        default:
            return state
    }
}

export default postReducer;