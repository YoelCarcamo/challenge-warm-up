import React, { useReducer } from 'react';
import axios from 'axios';

import PostContext from './postContext';
import postReducer from './postReducer'
import { CREAR_POSTEO, EDITAR_POSTEO, ELIMINAR_POSTEO, OBTENER_TODOS_LOS_POSTEOS, POSTEO_NO_ENCONTRADO, POSTEO_TRAIDO } from '../../types';

const PostState = props => {
    const stateInicial = {
        posteos: [],
        posteoTraido: {},
        mensaje: null,
        mensajeNro: 0
    }

    const [state, dispatch] = useReducer(postReducer, stateInicial)

    const obtenerTodosLosPosteos = async () => {
        try {

            //hacemos una peticion get para obtener los posteos
            const respuesta = await axios.get('https://jsonplaceholder.typicode.com/posts');

            dispatch({
                type: OBTENER_TODOS_LOS_POSTEOS,
                payload: respuesta.data
            })

        } catch (error) {
            console.log(error)
        }
    }


    const borrarPosteoPorId = async id => {
        try {
            //Hacemos la peticion para borrarlo
            await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)

            dispatch({
                type: ELIMINAR_POSTEO,
                payload: Number(id)
            })
            
        } catch (error) {
            console.log(error)
        }
    }

    const obtenerPosteoPorId = async id => {
        try {
            //Hacemos la peticion buscando el post por id
            const respuesta = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);

            dispatch({
                type: POSTEO_TRAIDO,
                payload: respuesta.data
            })
        }catch(error){
            console.log(error)

            dispatch({
                type: POSTEO_NO_ENCONTRADO
            })
        }
    }

    const crearPosteo = async posteo => {
        try {
            //hacemos la peticion para crear el posteo, y nos devuelve el posteo creado con su id
            const respuesta = await axios.post('https://jsonplaceholder.typicode.com/posts', posteo)

            dispatch({
                type: CREAR_POSTEO,
                payload: respuesta.data
            })
            
        } catch (error) {
            console.log(error.message)
        }
    }

    const editarPosteo = async posteo => {
        try {
            //hacemos la peticion para editar el posteo
            const respuesta = await axios.put(`https://jsonplaceholder.typicode.com/posts/${posteo.id}`, posteo);
            
            dispatch({
                type: EDITAR_POSTEO,
                payload: respuesta.data
            })
            
        } catch (error) {
            console.log(error.message)
        }
    }

    return ( 
        <PostContext.Provider
            value={{
                posteos: state.posteos,
                posteoTraido: state.posteoTraido,
                mensaje: state.mensaje,
                mensajeNro: state.mensajeNro,
                obtenerTodosLosPosteos,
                borrarPosteoPorId,
                obtenerPosteoPorId,
                crearPosteo,
                editarPosteo
            }}
        >
            {props.children}
        </PostContext.Provider>
     );
}
 
export default  PostState;