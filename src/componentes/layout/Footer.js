import React from 'react';

const Footer = () => {
    return ( 
        <footer className='bg-dark text-white text-center p-3'>
            <p>Challenges Warm Up</p>
        </footer>
     );
}
 
export default Footer;