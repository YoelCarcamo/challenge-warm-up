import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alerta/alertaContext';


import authContext from '../../context/autenticacion/authContext';
import PostContext from '../../context/post/postContext';

const Header = () => {

    const { cerrarSesion } = useContext(authContext);

    const { mensaje, mensajeNro } = useContext(PostContext);

    const { alerta, mostrarAlerta } = useContext(AlertaContext);

    const eventoCerrarSesion = () => {
        cerrarSesion();
    }

    useEffect(()=> {
        if(mensaje){
            //mostramos el alerta
            mostrarAlerta(mensaje)
        }

        // eslint-disable-next-line
    }, [mensajeNro])

    return ( 
        <nav className='bg-dark'>
            <div className='container d-flex flex-wrap justify-content-between p-3'>

                <Link to='/' className='logo h4'>Blog</Link>

                <ul className="nav">
                    <li className="nav-item">
                        <Link className="nav-link text-white" to="/">Inicio</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link text-white" to="/post/crear">Crear Post</Link>
                    </li>
                    <li>
                        <button className='btn btn-danger' onClick={eventoCerrarSesion}>Cerrar Sesion</button>
                    </li>
                </ul>

            </div>

            { !alerta ?
                null
            :
                <p className='bg-warning text-center h5 py-1'>
                    {alerta}
                </p>
            }
            

        </nav>
    );
}
 
export default Header;