import React from 'react';

const Spinner = () => {
    return ( 
        <section className='justify-content-center d-flex my-4'>
            <div className="spinner-border text-primary" role="status">
                <span className="sr-only"></span>
            </div>
        </section>
     );
}
 
export default Spinner;