import React, { useContext } from 'react';
import {Link }from 'react-router-dom'
import PostContext from '../../context/post/postContext';

const Post = ({posteo}) => {

    const {id, title} = posteo;

    const {borrarPosteoPorId} = useContext(PostContext)

    return ( 
        <article className='my-4 p-4 border'>
            <h5>{title}</h5>
            <div className='text-right mt-1'>
                <Link to={`/post/ver/${id}`}  className='btn btn-dark mr-2'>Ver más</Link>
                <Link to={`/post/editar/${id}`} className='btn btn-dark mr-2'>Editar</Link>
                <button onClick={() => borrarPosteoPorId(id)} className='btn btn-danger'>Eliminar</button>
            </div>
        </article>
    );
}
 
export default Post;